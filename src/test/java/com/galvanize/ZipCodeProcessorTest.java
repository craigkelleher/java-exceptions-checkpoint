package com.galvanize;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ZipCodeProcessorTest {

    // write your tests here
    @Test
    public void testValidZipCde() {
        final Verifier verifier = new Verifier();
        ZipCodeProcessor processor = new ZipCodeProcessor(verifier);
        String result = processor.process("80302");
        assertEquals("Thank you!  Your package will arrive soon.", result);
    }

    @Test
    public void testInvalidFormatLongZipCode() {
        final Verifier verifier = new Verifier();
        ZipCodeProcessor processor = new ZipCodeProcessor(verifier);
        String result = processor.process("9765432");
        assertEquals("The zip code you entered was the wrong length.", result);
    }

    @Test
    public void testInvalidFormatShortZipCode() {
        final Verifier verifier = new Verifier();
        ZipCodeProcessor processor = new ZipCodeProcessor(verifier);
        String result = processor.process("666");
        assertEquals("The zip code you entered was the wrong length.", result);
    }

    @Test
    public void testNoServiceZipCode() {
        final Verifier verifier = new Verifier();
        ZipCodeProcessor processor = new ZipCodeProcessor(verifier);
        String result = processor.process("12233");
        assertEquals("We're sorry, but the zip code you entered is out of our range.", result);
    }

    @Test
    public void testVerifier() throws InvalidFormatException, NoServiceException {
        final Verifier verifier = new Verifier();
        ZipCodeProcessor processor = new ZipCodeProcessor(verifier);
        verifier.verify("80302");
        assertThrows(InvalidFormatException.class,()->verifier.verify("12345678"));
        assertThrows(InvalidFormatException.class,()->verifier.verify("3210"));
        assertThrows(NoServiceException.class,()->verifier.verify("12233"));
    }
}